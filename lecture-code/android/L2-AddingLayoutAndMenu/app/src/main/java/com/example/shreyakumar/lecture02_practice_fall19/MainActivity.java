package com.example.shreyakumar.lecture02_practice_fall19;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Button;
        import android.util.Log;
        import android.widget.Toast;
        import android.content.Context;


public class MainActivity extends AppCompatActivity {

    // create handles that can be connected to view system elements
    private TextView mSearchResultsTextView;
    private TextView mDisplayTextView;
    private EditText mSearchBoxEditText;
    private Button   mSearchButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main); // R is the class that has all the resources information.

        // connecting handles to view system elements
        mSearchResultsTextView  = (TextView) findViewById(R.id.tv_results);
        mDisplayTextView        = (TextView) findViewById(R.id.tv_display_text);
        mSearchBoxEditText      = (EditText) findViewById(R.id.et_search_box);
        mSearchButton           = (Button)   findViewById(R.id.button_search);

        // detecting and responding to user event of button click
        mSearchButton.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v){

                        // this code executes when the button is clicked - closure
                        String searchText = mSearchBoxEditText.getText().toString();
                        Log.d("informational", "Search button clicked!" + searchText);
                        mSearchResultsTextView.setText("Searching for " + searchText);

                        Context c = MainActivity.this;
                        String msg = "Search clicked! " + searchText;
                        Toast.makeText(c, msg, Toast.LENGTH_LONG).show();

                                             }   // end of onClick method
                                         } // end of object View.OnClickListener
        ); // end of setOnClickListener invocation

    } // end of onCreate method
} // end of class